#pragma once

namespace Diglett {
    enum Languages {
        English = 0,
        French = 1,
        Spanish = 2,
        German = 3,
        Japanese = 16,
        Korean = 19
    };
}
