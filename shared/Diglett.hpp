#include "Format.hpp"
#include "Languages.hpp"
#include "LocalizationDocument.hpp"
#include "Parsing.hpp"
#include "Register.hpp"

#include "beatsaber-hook/shared/rapidjson/include/rapidjson/memorystream.h"

#define Language Diglett::Languages

typedef Diglett::LocalizationDocument Localization;